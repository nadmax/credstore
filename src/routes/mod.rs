mod handlers;

use handlers::{auth, health};

use axum::routing::{get, post};
use axum::Router;

pub fn create_routes() -> Router<()> {
    Router::new()
        .route("/", get(handlers::root()))
        .route("/health", get(health::health_check()))
        .route("/register", post(auth::register()))
        .route("/login", post(auth::login()))
}
