use axum::Json;
use serde_json::{json, Value};

pub fn health_check() -> Json<Value> {
    Json(json!({
        "status": "success",
        "message": "API is up"
    }))
}
