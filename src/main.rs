mod models;
mod routes;

use credstore::run;

#[tokio::main]
async fn main() {
    tracing_subscriber::fmt::init();

    run().await
}
