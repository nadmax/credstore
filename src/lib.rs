mod routes;

use std::net::SocketAddr;

pub async fn run() {
    let app = routes::create_routes();
    let addr = SocketAddr::from(([127, 0, 0, 1], 3000));

    tracing::debug!("🚀 Credstore API is up! Listening on: {}", addr);
    axum::Server::bind(&addr)
        .serve(app.into_make_service())
        .await
        .unwrap();
}
