pub mod auth;
pub mod health;

pub fn root() -> &'static str {
    "Welcome to Credstore API"
}
